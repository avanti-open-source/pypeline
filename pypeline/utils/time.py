from typing import Any, List
import datetime as dt

from loguru import logger
import pandas as pd


def limit_time(data: Any,
               start_date: dt.datetime,
               end_date: dt.datetime) -> Any:
    """ If :data: is a pd.Series or pd.Dataframe, sets a start date and an 
    end date.
    """
    if isinstance(data, pd.Series) or isinstance(data, pd.DataFrame):
        try:
            trimmed_data = data[data.index >= start_date]
            trimmed_data = trimmed_data[trimmed_data.index <= end_date]
            if len(trimmed_data) == 0:
                logger.error("The Series or DataFrame now has 0 values.")
                logger.error(f"Trimmed start data: {start_date}")
                logger.error(f"Trimmed end data: {end_date}")
                logger.error(f"Data to be trimmed: {data}")
                raise IndexError("The Series or DataFrame now has 0 values.")
            data = trimmed_data
        except TypeError:
            pass
    
    return data


def ensure_equal_indexes(series: List[pd.Series]) -> List[pd.Series]:
    """ Creates a pandas.Dataframe from all the timeseries to make the
    indexes equal. Then returns the series separated again.
    """
    df = pd.DataFrame(series)
    df = df.transpose()

    # Returning the series with the equal indexes.
    new_series = [df[col] for col in df]

    return new_series

