from concurrent.futures import ThreadPoolExecutor, as_completed
from pypeline import StrategyOperation


class BulkTest(StrategyOperation):

    def __init__(self, parameters,
                 since=None, until=None,
                 *args, **kwargs):
        """
        :param number_of_assets: integer. Number of assets per strategy
        :param start_date: str or datetime. Starting date of the backtest.
        :param end_date: str or datetime. Ending date of the backtest.
        """
        super().__init__(*args, **kwargs)
        self.parameters = {}
        self.parameters.update(parameters)
        self.since = since
        self.until = until

    def _run(self, strategies, data, threaded=False):
        """
        Calculate the strategy using a combination of timeseries

        :param strategies:
        :param data: list of lists. Each list will contain two timeseries.
        """
        # Threaded calculation.
        if threaded:
            with ThreadPoolExecutor(max_workers=5) as executor:
                futures = [executor.submit(self.execute_strategy, strategies[0], ts)
                           for ts in data]

                strategies = [f.result() for f in as_completed(futures)]

        # Non threaded calculation.
        else:
            strategies = [self.execute_strategy(strategies[0], ts)
                          for ts in data[0]]

        return {'multiple_strategy': strategies}

    def execute_strategy(self, strategy_builder, timeseries):
        """

        :param timeseries: pd.Series. The timeseries of the strategy calculation.
        :return: None
        """
        # Adding timeseries as a parameters.
        timeseries_dict = {
            'timeseries': timeseries,
        }
        self.parameters.update(timeseries_dict)

        # Building strategy.
        name_one = timeseries[0].name
        name_two = timeseries[1].name
        builder = strategy_builder(parameters=self.parameters, 
                                   name="{}_{}".format(name_one, name_two))
        strategy = builder.build()
        strategy.calculate(start_date=self.start_date,
                           end_date=self.end_date,
                           until=self.until)

        return strategy
