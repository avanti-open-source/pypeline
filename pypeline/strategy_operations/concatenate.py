from typing import Dict, Any, List

from loguru import logger
from pypeline import StrategyOperation
import pandas as pd


class ConcatenateResults(StrategyOperation):
    def _run(self, 
             strategies: Any,
             data: List[Dict],
             returns_name: str = 'returns',
             trades_name: str = 'trades'):

        keys = list(data[0].keys())

        results = {
            "_".join([pipeline.name for pipeline in strategy_tuple]): {
                "concatenated_returns": pd.Series(),
                "concatenated_trades": pd.DataFrame()
             } for strategy_tuple in zip(*[data[0][key] for key in keys])
        }

        strategies_number = len(data[0][keys[0]])
        
        returns_list = []
        trades_list = []
        for i in range(strategies_number):
            aux_returns_list = []
            aux_trades_list = []
            for k, sample in data[0].items():
                aux_returns_list.append(sample[i][returns_name])
                aux_trades_list.append(sample[i][trades_name])
            returns_list.append(aux_returns_list)
            trades_list.append(aux_trades_list)

        # Concatenating results
        results_keys = list(results.keys())
        for i, key in enumerate(results_keys):
            results[key]["concatenated_returns"] = pd.concat([results[key]["concatenated_returns"]] +
                                                             returns_list[i])
            results[key]["concatenated_trades"] = pd.concat([results[key]["concatenated_trades"]] +
                                                            trades_list[i])

        return {"concatenated_results": results}
