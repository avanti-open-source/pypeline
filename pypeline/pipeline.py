# -*- coding: utf-8 -*-
"""
The Pypeline class that is the starting point for any backtest.
It creates a framework or pipeline for adding,
transforming and filtering time series in order to test an algorithmic
trading strategy.

The main logic of the strategy's pipeline is the following:

    - The pipeline is made out of several steps.
        pipeline = [step_one, step_two, ... step_n]

    - A step could be constituted by one or many operations.
        step = [operation_one, operation_two, ..., operation_n]

    - Therefore, a pipeline could look like the following:
        pipeline = [[operation_one],
                    [operation_one, operation_two, operation_three],
                    ...,
                    [operation_one, operation_two]]

    - All the operations that constitute one step will be executed
    asynchronously. Therefore, for performance sake, it is advised to put in
    a single step all the operations that can be executed independently.
"""


import datetime as dt
from multiprocessing.pool import ThreadPool
from typing import Optional, List, Any, Dict, Union

from loguru import logger
import pandas as pd
import time
from .operation import Operation
from .inputdata import DataReader


class Pypeline():
    """
        Base class.
        The Pypeline class creates a framework or pipeline for adding,
        transforming and filtering time series in order to test an algorithmic
        trading strategy.

        Important note about the classes design:
        This class is inheriting from the class DataReader, making the inheritance
        tree more complex than what is recommended. It would be possible to use it
        by composition instead and simplify the architectural design. Nevertheless,
        that would make more complex the API of the system.
        We preferred to simplify the external programmers experience even if that
        increased the complexity of the system for the core developers.
    """

    def __init__(self,
                 operations_classes: Dict = None,
                 data_reader: Optional[DataReader] = None,
                 name: str = "unnamed",
                 *args, **kwargs):
        """
            Initiallizes the pipeline object with a start and ending date.
            An injected data reader could be used but uses the base DataReader
            class by default.
            :param
                operations_classes: The key of this dictionary are the name of the operations
                    and the values are the object (constructor)
                data_reader: This object is for read the data and save it
                name: the name of the pypeline
        """
        super().__init__(*args, **kwargs)
        self.pipeline = []
        if data_reader:
            self.data = data_reader
            logger.debug("New pypeline with custom data reader")
        else:
            self.data = DataReader()
            logger.debug("New pypeline with default data reader")
        self._steps = 1

        # Searchable dictionaries.
        self.name = name
        self.results = {}
        self.saved = {}
        # Execution time list of tuple, this save the execution time by step in a sorted way
        # (step_1 first then step 2 and so on), the tuple has form (name step, execution time)
        self.execution_time = []
        # create a list that is going to save the name of the operations that are added with
        # the add_operation method
        self.operations = []
        self.operations_classes = operations_classes

    def __getitem__(self, key):
        return self._search(key)

    def add_step(self,
                 step_operations: List[Operation],
                 apply_to: Union[List[str], List[List[str]]],
                 condition: Optional[Dict[str, str]] = None,
                 name: Optional[str] = None,
                 sample_type: Optional[str] = None):
        """
        Adds a step to the pipeline. The step is a list of operations
        that will be processed later by the pipeline.
        The :apply_to: list are the names of the inputs of the operations
        of the step.

        TODO: capability of applying each operation to a different input data.
        """
        # Checking apply_to is an iterable.
        if not isinstance(apply_to, list):
            raise TypeError("apply_to must be a list.")

        # Creating the step.
        step_name = name or "step_" + str(self._steps)
        step = {'name': step_name,
                'operations': [],
                'processed': False,
                'sample_type': sample_type}
        self._steps += 1
        # if the apply to is an empty list or the if the elements inside the apply to
        # doesn't are list (this is the case List[str]) convert it to a List[List[str]]
        # in other case will stay equal
        if len(apply_to) == 0 or type(apply_to[0]) != list:
            # in the case that exist multiple operation but a unique list
            # use * len(step_operations) to put the same apply to to every one of them
            apply_to = [apply_to] * len(step_operations)
        # Adding the operations.
        for i, operation in enumerate(step_operations):
            operation_dict = {}
            # Adding the operation attributes.
            if operation.name:
                operation_dict['name'] = operation.name
            operation_dict['operation'] = operation
            operation_dict['apply_to'] = apply_to[i]
            if condition and list(condition.keys())[0] == operation.name:
                operation_dict['condition'] = condition
            step['operations'].append(operation_dict)

        # Appending the step(s) to the pipeline.
        self.pipeline.append(step)
        logger.debug(f"New step with name: {step_name}")

    def add_operation(self,
                      name: str,
                      parameters: Dict,
                      apply_to: List[str]):
        # create an operation as a dict
        operation = {
            'name': name,
            'parameters': parameters,
            'apply_to': apply_to
        }
        # save it in the operations attribute
        self.operations.append(operation)

    def organize_operations(self):
        """
            :abstraction
                Every iteration create a new step until there isn't operations
                the steps are created following  this logic:
                The iteration 'zero' create some data that is the data that was added manually
                not the data created by the operations.
                The iteration one take all those operations that only need the data of the iteration zero
                and grouped together to create one parallel step (can be a unique operation by step)
                and add the result name of those operation to the actual know data and delete those operations
                from the unprocessed_operations.
                The second iteration make the same like the first but taking the actualized data into account
                and only verify with the unprocessed operations
                and so on...
        """
        # initial data
        actual_data = set(self.data.data.keys())
        # initial operations that are "unprocessed"
        unprocessed_operations = self.operations
        while unprocessed_operations:
            # create 4 list one the first is for apply to, the second for the names of the operations
            # the third for the parameters and the last for the result_names
            applies_to, operation_names, operations_parameters, new_data_to_add = [], [], [], []
            for operation in unprocessed_operations:
                if set(operation['apply_to']).issubset(actual_data):
                    new_data_to_add.append(operation['parameters']['result_name'])
                    applies_to.append(operation['apply_to'])
                    operation_names.append(operation['name'])
                    operations_parameters.append(operation['parameters'])

            if not new_data_to_add:
                raise ValueError("There are operations that dependent on each other" +
                                 " or there isn't the necessary data")
            # add the result names to the data (this is equivalent to process the operations of a step)
            actual_data.update(new_data_to_add)
            # delete the operations that has been "processed"
            unprocessed_operations = [operation for operation in unprocessed_operations
                                      if operation['name'] not in operation_names]

            # instance the operations with the dictionary of classes and crete a list with them
            step_operations = [self.operations_classes[name](**parameters)
                               for name, parameters in zip(operation_names, operations_parameters)]
            logger.info("Operations in step {}: {}".format(self._steps, operation_names))
            # add the step
            self.add_step(step_operations=step_operations,
                          apply_to=applies_to)

    def calculate(self,
                  start_date: dt.datetime,
                  end_date: dt.datetime,
                  in_sample_end: Optional[dt.datetime] = None,
                  until: Optional[str] = None):
        """ Processes all the stages of the strategy's pipeline.
        If a step in the pipeline containes several stages, each stage
        is processed in a concurrent way.
        """
        # Logging.
        message = f"Pipeline calculation "
        if until:
            message += f"until step {until} "
        message += f"from {start_date} to {end_date}. "
        if in_sample_end:
            message += f"In-sample ends at {in_sample_end}"
        logger.debug(message)
        logger.debug(f" Showing pipeline: {self.pipeline}")
        # organize the operations that was added to pypeline
        # if not exist operations added this method don't do anything
        self.organize_operations()

        # Creating a threads pool.
        pool = ThreadPool(processes=4)

        # Processing each step of the pipeline sequentially.
        for step in self.pipeline:
            if not step['processed']:
                # Processing each operation of the step concurrently.
                futures = []
                # take the instant of time in which the operation begins
                start_time_step = time.time()
                for operation in step['operations']:
                    logger.debug(f"Operation: {operation}")
                    # Getting the necessary data for the operation.
                    data = [self[x] for x in operation['apply_to']]
                    # Limiting dates according to the type of sample.
                    self._limiting_time(operation=operation['operation'],
                                        sample_type=step['sample_type'],
                                        start_date=start_date,
                                        end_date=end_date,
                                        in_sample_end=in_sample_end)

                    # Running the operation in a new thread.
                    future = pool.apply_async(func=operation['operation'].run,
                                              args=(data,))
                    futures.append(future)
                # Getting the results of  all the threads before proceding
                # with the next step.
                for i, future in enumerate(futures):
                    result = future.get()
                    # Storing the result.
                    self._store_results(result=result,
                                        step=step,
                                        operation_number=i)

                # save the execution time of the steps in the execution time list
                # calculate the execution time as the difference between the begin
                # and the finish moment of the operation
                self.execution_time.append((step['name'], time.time() - start_time_step))
                # Checking if the conditions were met.

                # Mark the step as processed.
                step['processed'] = True

            # Breaking the pipeline processing if specified.
            if step['name'] == until:
                break
        
        logger.info("Pypeline processed successfully.")
        logger.info("Table of the executions time by step: ")
        for name_step, execution_time in self.execution_time:
            logger.info("{:<7} processing time: {:.20f} s".format(name_step, execution_time))

    def _divide_dates(self,
                      start_date: dt.datetime,
                      end_date: dt.datetime,
                      divisions: int,
                      samples_proportion: float) -> Dict:
        """Creates the dictionary that contain the samples limits.
        Divides the complete sample in <:division:> parts, and each
        part is divided in insample and outsample based on the :proportion:.
        """
        # Initiallizing base dict and variables.
        dates = {}
        total_time = end_date - start_date
        proportion = samples_proportion / (1 - samples_proportion)
        total_divisions = proportion + divisions
        time_division = total_time / total_divisions

        # Calculating and naming each division.
        samples_name = 'sample_'
        for i in range(divisions):
            sample_name = samples_name + str(i + 1)
            dates[sample_name] = {}
            dates[sample_name]['sample_start'] = start_date + \
                time_division * i
            dates[sample_name]['in_sample_end'] = dates[sample_name]['sample_start'] + \
                time_division * proportion
            dates[sample_name]['sample_end'] = dates[sample_name]['in_sample_end'] + time_division
        logger.info(f"Test dates are: {dates}")

        return dates

    def _limiting_time(self,
                       operation: Operation,
                       sample_type: str,
                       start_date: dt.datetime,
                       end_date: dt.datetime,
                       in_sample_end: dt.datetime) -> Any:
        """ Trims a timeseries based on a :start_date: and :end_date:
        or if it is insample or outsample.
        """
        if sample_type == 'in_sample':
            operation.start_date = start_date
            operation.end_date = in_sample_end
        elif sample_type == 'out_sample':
            operation.start_date = in_sample_end
            operation.end_date = end_date
        else:
            operation.start_date = start_date
            operation.end_date = end_date

        return operation

    def _preprocess_data(self,
                         data: List[Any],
                         start_date: Optional[dt.datetime] = None,
                         end_date: Optional[dt.datetime] = None) -> List[Any]:
        """ This method is executed before the pipeline is processed by calculate().
        It sets a fixed start and end date for the timeseries and guarantees
        that the sizes are the same.
        """
        # Creates a pandas.Dataframe from all the timeseries to make the
        # indexes equal.
        series = [x for x in data if isinstance(x, pd.Series)]
        df = pd.DataFrame(series)
        df = df.transpose()

        # Checking for bug when two data elements have the same name.
        if len(df.columns.unique()) != len(df.columns):
            raise RuntimeError(
                "Two columns have the same name. An operation must be naming two series equally.")

        # Sets a start date and an end date.
        if start_date:
            df = df[df.index >= start_date]
        if end_date:
            df = df[df.index <= end_date]

        # Updating the timeseries with the preprocessed ones.
        result = []

        for ts in data:
            if isinstance(ts, pd.Series):
                try:
                    result.append(df[ts.name])
                except ValueError:
                    raise ValueError(
                        "The Series {} does not have name".format(ts))
            else:
                result.append(ts)

        return result

    def _search(self, name: str) -> Any:
        """ Searchs the data reader object (self.data) and the results 
        of the operations looking for a named object.
        """
        # Looking for the desired object into the built-in searchable objects.
        if name == 'saved':
            return self.saved

        # Looking for the desired object into the data reader.
        try:
            return self.data[name]
        except KeyError:
            pass

        # Looking for the desired object into the self.results attribute.
        try:
            return self.results[name]
        except KeyError:
            message = "The element {} doesn't exist".format(name)
            raise AttributeError(message)

    def _store_results(self,
                       result: Any,
                       step: List[Any],
                       operation_number: int,
                       save: bool = False,
                       save_suffix: str = ''):
        """Stores the results of an operation in the results
        dictionary and the name in the pipeline.
        """
        operation = step['operations'][operation_number]

        # Storing the results v2.
        operation['result'] = []
        for name, value in result.items():
            operation['result'].append(name)
            self.results[name] = value
            logger.debug(f"New result with name {name} was stored")
            # If save is True, saves also the result in a dictionary
            # using a name that won't be replaced.
            if save:
                save_name = name + '_' + save_suffix + '_' + step['name']
                self.saved[save_name] = value
                logger.debug(f"{save_name} saved")

    def train_predict(self,
                      start_date: dt.datetime,
                      end_date: dt.datetime,
                      divisions: int = 1,
                      samples_proportion: float = 0.8):
        """ Calculation method that takes into account the 'sample_type'
        parameter of each step.
        Its objective is to divide the calculations into in sample and
        out sample.
        """
        dates = self._divide_dates(start_date=start_date,
                                   end_date=end_date,
                                   divisions=divisions,
                                   samples_proportion=samples_proportion)

        # Walking forward through the first two steps of the backtest.
        for i in range(divisions):
            sample_dates = dates['sample_' + str(i + 1)]
            self.calculate(start_date=sample_dates['sample_start'],
                           end_date=sample_dates['sample_end'],
                           in_sample_end=sample_dates['in_sample_end'])

        logger.info("Train/Predict finished.")



