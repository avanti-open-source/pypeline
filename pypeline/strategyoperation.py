from typing import List, Any, Optional, Union

from pypeline.utils import limit_time
from loguru import logger

from .operation import Operation
from .pipeline import Pypeline
from .strategybuilder import StrategyBuilder


class StrategyOperation(Operation):
    """A higher level of abstraction than Operation.
    It is used by StrategyManager as Operation is used 
    by Pypeline.
    """
    def run(self,
            strategies: Union[List[Pypeline], List[StrategyBuilder], None] = None,
            data: Optional[List[Any]] = None) -> Any:
        """ Main method. It is called by Strategy.calculate() when processing
        all the steps of the pipeline of the strategy.
        It uses _run() for making the operation calculations.
        The main difference with the run() method from the class
        Operation is that it also accepts as input list of strategies that may
        be inheritants of Pypeline or StrategyBuilder.
        """
        # Limiting dates.
        # if data:
        #     data = [limit_time(x, self.start_date, self.end_date)
        #             for x in data]

        # Running default calculation method.       

        if self.kind:
            method = getattr(self, self.kind)
            result = method(strategies=strategies, data=data)
        else: 
            result = self._run(strategies=strategies, data=data)

        return self._attach_name(result)

    def _run(self, strategies, data):
        """
        Default method of an operation.
        """
        raise NotImplementedError