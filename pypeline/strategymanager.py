"""The strategy manager uses a similar abstraction as the
Pypeline base class to deal with a higher level
of strategy manipulations.
As the Pypeline class process operations in its
pipeline, the strategy manager can also process
strategy operations.
A walkforward analysis is an example of the processes
that can be done with the strategy manager.
"""

from typing import Optional, Union, List, Dict
import datetime as dt

from loguru import logger

from .config import VERBOSE
from .pipeline import Pypeline
from .strategybuilder import StrategyBuilder


class StrategyManager(Pypeline):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.strategies = {}
        logger.debug("Pypeline elevated to Strategy Manager")

    def add_step(self,
                 use_strategy: Union[List[Pypeline], List[StrategyBuilder], None] = None, 
                 *args, **kwargs):
        """Extends the add_step method for adding strategies that will be used
        in the StrategyManager pipeline.

        Raises TypeError when use_strategy is not a list.
        """

        super().add_step(*args, **kwargs)

        # Adding strategies to the step.
        step = self.pipeline[-1]
        step['use_strategy'] = use_strategy

    def add_strategy(self, 
                     strategy: Union[List[Pypeline], List[StrategyBuilder]], 
                     name: str):
        self.strategies[name] = strategy
        logger.info(f"Strategy added with name {name}")

    def calculate(self,
                  start_date: dt.datetime,
                  end_date: dt.datetime,
                  in_sample_end: Optional[dt.datetime] = None,
                  from_step: Optional[str] = None,
                  to_step: Optional[str] = None,
                  process_anyway: bool = False,
                  save: bool = False,
                  save_suffix: str = ''):
        """Processes all the steps of the strategy manager pipeline.        
        """
        # Logging.
        message = f"Strategy Manager calculation "
        if from_step:
            message += f"from step {from_step} "
        if to_step:
            message += f"until step {to_step} "
        message += f"from {start_date} to {end_date}. "
        if in_sample_end:
            message += f"In-sample ends at {in_sample_end}"
        logger.debug(message)
        logger.debug(f" Showing pipeline: {self.pipeline}")
        
        # Processing each step of the pipeline sequentially.
        from_step_flag = False if from_step else True

        for step in self.pipeline:
            # Determining if this step should be processed.
            process_step = False
            if not step['processed'] or process_anyway:
                process_step = True
            if from_step and step['name'] == from_step:
                from_step_flag = True

            # Starting the step processing.
            if process_step and from_step_flag:

                # Processing each operation of the step not concurrently.
                for i, operation in enumerate(step['operations']):
                    # Getting the necessary data and strategies for the operation.
                    data = [self[x] for x
                            in operation['apply_to']]
                    try:
                        strategies = [self[x] for x in step['use_strategy']]
                    except TypeError:
                        strategies = None

                    # Limiting dates according to the type of sample.
                    self._limiting_time(operation=operation['operation'],
                                        sample_type=step['sample_type'],
                                        start_date=start_date,
                                        end_date=end_date,
                                        in_sample_end=in_sample_end)

                    # Running the opperation in a new thread.
                    try:
                        result = operation['operation'].run(data=data,
                                                            strategies=strategies)
                    # If the operation is not a strategy operation try with a 
                    # regular operation.
                    except TypeError:
                        result = operation['operation'].run(data=data)

                    # Checking if the conditions of the operation was met.
                    try:
                        condition = operation['condition']
                        operation_name = list(condition.keys())[0]
                        if condition[operation_name] != result:
                            if VERBOSE > 1:
                                print("Condition for the operation \"{}\" wasn't met".format(
                                    operation_name))
                            result = False
                    except KeyError:
                        pass

                    # Storing the result.
                    self._store_results(result=result,
                                        step=step,
                                        operation_number=i,
                                        save=save,
                                        save_suffix=save_suffix)

                # Mark the step as processed.
                step['processed'] = True

            # Breaking the pipeline processing if specified.
            if step['name'] == to_step:
                break

        logger.info("Stratagy Manager processed succesfully")

    def _search(self, name: str):
        """
        Extends the _search() method from the parent class.
        """
        try:
            return super()._search(name)
        except AttributeError:
            try:
                return self.strategies[name]
            except KeyError:
                message = "The element {} doesn't exist".format(name)
                raise AttributeError(message)

    def walkforward_analysis(self,
                             start_date: dt.datetime,
                             end_date: dt.datetime,
                             breaks: Dict[str, str],
                             divisions: int = 1,
                             samples_proportion: float = 0.8):
        """
        Runs a walkforward analysis based on the dates of the
        :breaks: dictionary.
        The format of :breaks: is {'insample_end': <step_name>,
                                   'outsample_start': <step_name>,
                                   'outsample_end': <step_name>,
                                   'final': <step_name>}

        It also takes in charge the management of the insample and
        outsample periods.
        """
        # Initial print.
        logger.info(f"Walk Forward Analysis started with {divisions} division(s) and insample proportion of {samples_proportion}")

        # Defining breaking points of training and test periods.
        dates = self._divide_dates(start_date=start_date,
                                   end_date=end_date,
                                   divisions=divisions,
                                   samples_proportion=samples_proportion)
        breaks_dict = {'insample_end': None,
                       'outsample_start': None,
                       'outsample_end': None,
                       'final': None}
        breaks_dict.update(breaks)

        for i in range(divisions):
            # Train period calculations.
            sample_name = 'sample_{}'.format(i + 1)
            sample = dates[sample_name]
            logger.info(f"Testing sample number {i + 1}")

            self.calculate(to_step=breaks_dict['insample_end'],
                           start_date=sample['sample_start'],
                           end_date=sample['in_sample_end'],
                           process_anyway=True)

            # Test period calculations.
            logger.info(f"In sample test finished. Starting out sample test.")
            self.calculate(from_step=breaks_dict['outsample_start'],
                           to_step=breaks_dict['outsample_end'],
                           start_date=sample['in_sample_end'],
                           end_date=sample['sample_end'],
                           process_anyway=True,
                           save=True,
                           save_suffix=sample_name)
            logger.info(f"Out sample test finished.")

        # Processing the rest of the backtest.
        if breaks_dict['final']:
            logger.info(f"Processing final steps.")
            self.calculate(start_date=start_date,
                           end_date=end_date,
                           from_step=breaks_dict['final'])

        logger.info(f"Walk forward completed succesfully.")
