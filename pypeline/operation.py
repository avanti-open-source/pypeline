# -*- coding: utf-8 -*-
"""
The Operation class is used by the Pypeline class when processing the pipeline.
The Pypeline class is agnostic of the operations to be made and the data
to be used. It only knows that all the calculations are made using the method
run()

The Operations are the unit of processing of the backtesting framework.

The StrategyOperation is used by the StrategyManager in the same way
an Operation is used by Pypeline.
"""

from typing import Optional, List, Any, Dict
import datetime as dt

from pypeline.utils import limit_time


class Operation:
    """
    Base class for all the operations performed in a strategy.
    An operation could be a transformation, a filter, an addition or any other
    calculation over data in order to backtest a strategy.
    """

    def __init__(self,
                 name: Optional[str] = None,
                 kind: Optional[str] = None,
                 result_name: Optional[str] = None,
                 start_date: Optional[dt.datetime] = None,
                 end_date: Optional[dt.datetime] = None,):
        """
            Initiallizes an operation giving to it and its results a name.
            The :kind: parameter is used for choosing the method to be ran to
            make the operation calculations (See the run() method for more
            info).
        """
        self.name = name
        self.kind = kind
        if kind and not name:
            self.name = kind
        self.result_name = result_name
        self.start_date = start_date
        self.end_date = end_date

    def _attach_name(self, result: Any) -> Any:
        """
            This method is called before returning the result of an operation.
            Sets the attribute name to the result if it is a timeseries.
        """
        # If a result name was set.
        if self.result_name:

            # Attaching name v2.
            if isinstance(self.result_name, list):
                result = {k: v for k, v in zip(
                    self.result_name, result.values())}
            elif isinstance(self.result_name, str):
                result = {self.result_name: list(result.values())[0]}

            else:
                raise TypeError("Unknown type for result_name")

        return result

    def run(self, data: List[Any]) -> Any:
        """
            Main method. It is called by Strategy.calculate() when processing
            all the steps of the pipeline of the strategy.
            By default, it uses _run() for making the operation calculations.
            If there is a self.kind, it calls a method with the same name as
            self.kind.
        """
        # Limiting dates.
        data = [limit_time(x, self.start_date, self.end_date)
                for x in data]

        # Gets the method that has to be called according to the chosen operation type.
        if self.kind:
            method = getattr(self, self.kind)
            result = method(data)

        # In case there's no setted type, use the default _run method.
        else:
            try:
                result = self._run(data)
            except TypeError:
                result = self._run(*data)

        return self._attach_name(result)

    def _run(self, data: Any) -> Dict[str, Any]:
        """
            Default method of an operation.
            The result must be a dictionary with the name
            of the default name of the result as key and the
            result as value. An operation can return many
            results in one dictionary.
        """
        raise NotImplementedError

        

