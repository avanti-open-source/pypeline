# -*- coding: utf-8 -*-
"""

"""
from matplotlib import pyplot as plt
from pypeline import Operation
import pandas as pd
from numpy import ndarray
from copy import deepcopy
from typing import Dict, Tuple, Set, List


class LinearPlot(Operation):
    """ Creates a simple linear plot using matplotlib.
    Allows to plot cumulative values, thought specially for
    returns plot.
    """
    def __init__(self, name: str = "", path: str = "", save: bool = False,
                 size: Tuple[int, int] = (12, 8), neg_plot: Set[str] = None,
                 subplots: bool = False, layout: Tuple[int, int] = (1, 1),
                 constants_name: List[str] = None, sub_title=False,
                 groups_subplot: Dict[str, str] = None,
                 *args, **kwargs):
        """
        :param path: optional string
        :param neg_plot: set of strings. It's used to know which series need his negative part.
        :param save: boolean. if True then the "path" parameter is obligatory.
        :param size: tuple.
        :param subplots: boolean. Indicate if there must to be subplots.
        :param layout: optional tuple useful only with subplots.
        :param constants_name: list useful to put a name to the constants. this list most follow the order
        in which the constant are in timeseries.
        :param sub_title: used to indicate if it is necessary to put name to every subplot.
        :param groups_subplot: One of the more important parameters for the use of subplots
        it must be a dictionary which contain the name of the timeseries which are the key of the
        dictionary and it has to have like value the group which you want this timeseries belong to.
        So for example {'mean': 'b', 'negative_mean': 'b', 'std': 'c'}
        mean and negative mean will belong to the same group and this imply that they are going to be plotted
        in the same subplot and this value also indicate the name of every subplot.
        this dictionary is used in a groupby method of dataframes.
        """
        super().__init__(*args, **kwargs)
        self.save = save
        self.path = path
        self.size = size
        self.name = name
        self.subplots = subplots
        self.layout = layout
        self.sub_title = sub_title
        self.neg_plot = {}
        if neg_plot is not None:
            self.neg_plot = neg_plot
        self.constants_name = []
        if constants_name is not None:
            self.constants_name = constants_name
        self.groups_subplot = {}
        if groups_subplot is not None:
            self.groups_subplot = groups_subplot

    def _run(self, timeseries: List[pd.Series]) -> Dict:
        # initialization of the list for series and for constants
        timeseries_list = []
        constants_list = []
        constants_name = deepcopy(self.constants_name)
        # by default the constant will have only a number like name
        # this is useful given that the constant don't have an id or name by default
        if len(constants_name) == 0:
            constants_name = [str(i) for i in range(len(timeseries))]
        constant_act = 0

        for i, series in enumerate(timeseries):
            # if the feature is a timeseries then it will saved in timeseries_list
            # if the feature is a constant then it will saved in constant_list
            if isinstance(series, pd.Series):
                timeseries_list.append(series)
                # if the name is inside neg_plot add the negative series
                if series.name in self.neg_plot:
                    series_negative = -series
                    series_negative.name = "negative_" + series.name
                    timeseries_list.append(series_negative)
            else:
                # constants_list.append((series, series.name))
                constants_list.append((series, constants_name[constant_act]))
                # if the name is inside neg_plot add the negative constant
                if constants_name[constant_act] in self.neg_plot:
                    constants_list.append((-series, "negative_" + constants_name[constant_act]))
                constant_act += 1

        # creating dataframe with timeseries
        timeseries_df = pd.DataFrame(timeseries_list).T

        # appending constant as columns in the dataframe
        for constant, name in constants_list:
            timeseries_df[name] = constant

        # creating the groups of the dataframe
        # this loop fill the dictionary with the names of the timeseries that are not inside of it
        # there are two cases for the filling
        # 1 when self.subplots is true then create a new group for every timeseries that is not inside
        # 2 when self.subplots if false then create a unique group for every timeseries that is not inside

        groups_subplot = deepcopy(self.groups_subplot)
        for name in timeseries_df.columns:
            if name not in groups_subplot:
                if self.subplots:
                    groups_subplot[name] = name
                else:
                    groups_subplot[name] = ''

        # creating a dataframe grouped base on groups_subplot
        grouped_df = timeseries_df.groupby(timeseries_df.columns.map(groups_subplot),
                                           axis=1)

        # name of the plot
        plot_name = "{} Multiplot".format(self.name)

        # total number of plots
        total_plots = len(grouped_df.size())

        # used to validate if it is necessary to put legends in the plot
        # true by default, false if there is only one graphic
        legend = True

        # only for one timeseries or constant. original code,
        if len(timeseries_list) + len(constants_list) == 1:
            plot_name = timeseries[0].name
            legend = False

        # layout for the subplots useful even with one plot
        layout = self.layout
        # when the layout is not specified for the subplots, create one adapted by default for the plots
        if layout == (1, 1) and self.subplots:
            layout = (total_plots, 1)

        # creating the subplots that can be one
        plot = plt.subplots(figsize=self.size,
                            nrows=layout[0],
                            ncols=layout[1])
        general_plot, subs_plot = plot
        # setting the tittle
        general_plot.suptitle(plot_name)
        # creating an iterable object in the case when there is only one plot
        subs_plot = [subs_plot]
        # using flatten because by default, when there is more than one plot it is a numpy ndarray
        if isinstance(subs_plot[0], ndarray):
            subs_plot = subs_plot[0].flatten()
        # creating the grouped subplots one by one
        # this is a generalization for every number of plots
        # all the subplots are invisible until they pass for the second cycle, this is for avoid see
        # plots without data in the subplots
        for sub in subs_plot:
            sub.set_visible(False)
        sub_plots_grouped = zip(grouped_df.groups.keys(), subs_plot)
        for i, (key, sub) in enumerate(sub_plots_grouped):

            sub.set_visible(True)
            # plotting the subplot for a specific group
            sub.plot(grouped_df.get_group(key))
            # set the tittle in the subplot
            if self.sub_title:
                sub.set_title(key)
            # using legends if necessary
            if legend:
                sub.legend(grouped_df.get_group(key).columns, loc="upper right")
            # putting grids in the subplot
            sub.grid(True)

        # save the plot if self.save true else show it
        if self.save:
            plt.savefig(self.path + "/" + "{}Multiplot".format(self.name) + ".png")
        else:
            plt.show()

        return {"plot": plot}

    def cumulative(self, timeseries):
        """Plots the cumulative chart based on a returns timeseries.
        """
        # Cumulative timeseries.
        cumulative = (timeseries[0] + 1).cumprod()

        # Plotting.
        ax = cumulative.plot(title='Cumulative Returns', figsize=self.size,
                             grid=True, legend=False)
        ax.set_xlabel("Time")
        ax.set_ylabel("Profits percentage")
        plt.show()

        return {'cumulative_plot': ax}
