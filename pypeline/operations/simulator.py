"""

"""

from pypeline import Operation


class TradingSimulator(Operation):

    def __init__(self, capital, transaction_fee, market,
                 *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.transaction_fee = transaction_fee
        self.capital = capital

        # Class attributes.
        self.market_conditions = {
            'forex': 'percentage',
            'equities': 'fixed_fee'
        }

        # Setting market conditions.
        try:
            self.fees_type = self.market_conditions[market]
        except KeyError as error:
            raise KeyError(
                "{} market is not available.".format(market)) from error

    def _run(self, dataframe):
        """Calculates the PnL (Profit and Loss) using an initial capital.

        Parameters
        ----------
        dataframe : pandas.DataFrame
            Trades dataframe with a column named "return".

        Returns
        -------
        dict
            pandas.Series with the PnL as the value.
        """

        if self.fees_type == 'percentage':
            dataframe['return'] = dataframe['return'] - self.transaction_fee
        dataframe['cumulative'] = dataframe['return'].cumprod()
        dataframe['pnl'] = dataframe['cumulative'] * self.capital
        if self.fees_type == 'fixed_fee':
            dataframe['pnl'] = dataframe['pnl'] - self.transaction_fee
        dataframe = dataframe.set_index('exit_time')

        return {'pnl': dataframe['pnl']}
