"""Wrappers for using basic stats such as mean and std as an Operation
inheritant.

"""


from pypeline import Operation


class Mean(Operation):
    def _run(self, timeseries):
        return {'mean': timeseries[0].mean()}


class StandardDeviation(Operation):
    def _run(self, timeseries):
        return {'standard_deviation': timeseries[0].std()}
