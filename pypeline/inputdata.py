"""Class that reads input data from many sources.
To this moment, it is able to read from:
 - CSV files.
"""
from typing import Optional, Union, List, Iterable, Callable, Any

import pandas as pd
from loguru import logger

from .config import VERBOSE


class DataReader:
    """Base class.
    Reads input data from many sources.
    It can be extended to add new sources.
    """

    def __init__(self, *args, **kwargs):
        """
        Sets the method to be called for each data source.
        """
        self.data = {}
        self.sources = {
            "csv": "add_data_from_csv",
            "variable": "add_data_from_variable"
        }

    def __getitem__(self, key):
        return self.data[key]

    def __setitem__(self, key, value):
        self.data[key] = value

    def add_data(self, source: str, *args, **kwargs):
        """
        Wrapper for all the other time series methods.
        Uses :source: to know which method to use.
        """
        if source in self.sources.keys():
            method_name = self.sources[source]
            method = getattr(self, method_name)
            method(*args, **kwargs)
        else:
            raise IndexError("The source doesn't exist")
            
    def add_data_from_csv(self,
                          path: str,
                          name: Optional[str] = None,
                          use_percentage: bool = False,
                          header: Union[int, List[int]] = None,
                          index_col: Union[int, Iterable] = 0,
                          usecols: Union[Iterable, Callable, None] = None,
                          parse_dates: bool = True,
                          **kwargs):
        """
        Reads input data from a csv file using a :path: and assigning a :name:.
        Adds it to the input data of the pipeline.
        Uses pandas.read_csv for reading csv files. Refer to pandas.read_csv
        documentation for deeper information about the parameters.

        Here's a synthesis of the funcionality of some pandas.read_csv() parameter:
        - :use_percentage: Instead of using the original series, the percentage 
        difference between values is calculated and returned. Mostly used for turning
        price series into return series.
        - :index_col: Column to use as the row labels of the DataFrame.
        - :usecols: Subset of the columns to be used. 
        
        For reference to all the parameters and details, go to pandas.read_csv() 
        documentation.
        """
        # Setting name of the data.
        if not name:
            name = name = path.split('/')[-1].split('.')[0]

        # Reading .csv file.
        data = pd.read_csv(filepath_or_buffer=path,
                           header=header,
                           index_col=index_col,
                           usecols=usecols,
                           parse_dates=parse_dates,
                           **kwargs)

        # Giving name to the index and to the time series.
        data.index.name = 'index'
        data.columns = [name]

        # Applying pct_change if use_percentage is True. Useful for turning
        # price series into return series.
        if use_percentage:
            data[name] = data[name].pct_change()
            data.fillna(0, inplace=True)

        # Adding the data to the strategy's set of data.
        self.data[name] = data[name]
        logger.info(f"New data added with name: {name}")

    def add_data_from_variable(self, data: Any, name: str):
        """
        Adds a data from an already available variable and assigns a name.
        """
        self.data[name] = data
        logger.info(f"New data added with name: {name}")
