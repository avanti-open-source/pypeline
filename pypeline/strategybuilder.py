"""Builder class for generating strategies.

"""

import datetime as dt
from typing import Optional
from loguru import logger
from .pipeline import Pypeline


class StrategyBuilder:
    """ Base class.
    The inheritants of this class should implement each of the steps
    of the strategy. Normally, each step will be implemented
    usign the methods step_1(), step_2(), etc. But the strategy logic
    can also be implemented overriding only the strategy_rules() method.
    This class is in charge of providing a complete parameterized
    strategy to any other class in the system.
    """

    def __init__(self,
                 parameters: dict,
                 name:  Optional[str] = "unnamed",
                 calculate: bool = False,
                 strategy: Optional[Pypeline] = None,
                 start_date: Optional[dt.datetime] = None,
                 end_date: Optional[dt.datetime] = None,
                 *args,
                 **kwargs):
        """
            Initialize the builder with a specific set of parameters.
            self.strategy is the main attribute as it will be returned
            by the build() method.
            Optionally, the strategy can be executed (pypeline.calculate())
            before returning it.
            Also optionally, an already instantiated strategy can be injected.
        """
        self.parameters = parameters
        self._calculate = calculate
        self.start_date = start_date
        self.end_date = end_date
        self.name = name
        if strategy:
            self.strategy = strategy
        else:
            self.strategy = Pypeline()

    def build(self, 
              since: Optional[dt.datetime] = None,
              until: Optional[dt.datetime] = None) -> Pypeline:
        """Builds the strategy and returns it.
        """
        self.strategy_rules(since=since, until=until)

        if self._calculate:
            self.strategy.calculate(start_date=self.start_date,
                                    end_date=self.end_date)
        self.strategy.name = self.name
        return self.strategy

    def strategy_rules(self,
                       since: Optional[str] = None,
                       until: Optional[str] = None):
        """
            Executes sequentially all the steps of the strategy.
            It can be overriden and place the complete logic inside
            this method and executes the add methods of the strategy
            this is in the case when the strategy use the intelligent method
            to add operations but you can combine steps with adds methods
        """
        # Getting the steps using object introspection.
        steps = [x for x in dir(self) if x.startswith("step")]
        steps.sort()

        # Filtering from "since" to "until".
        if since:
            index = steps.index(since)
            steps = steps[index:]
        if until:
            index = steps.index(until) + 1
            steps = steps[:index]

        # Building each step.
        for step in steps:
            step_method = getattr(self, step)
            step_method()

        # Getting the adds using object introspection.
        adds = [x for x in dir(self) if x.startswith("add")]
        # Calling each add method.
        for add in adds:
            add_method = getattr(self, add)
            add_method()
