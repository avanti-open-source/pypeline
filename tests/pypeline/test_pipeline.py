import pandas as pd
import datetime as dt

from pypeline import Pypeline
from pypeline.config import TEST_DIR


class TestPypeline:
    def test_divide_dates(self):
        pype = Pypeline()
        
        division = pype._divide_dates(start_date=dt.datetime(2018, 1, 1),
                                      end_date=dt.datetime(2018, 1, 7),
                                      divisions=3,
                                      samples_proportion=0.75)

        division_dict = {
            'sample_1': {
                'sample_start': dt.datetime(2018, 1, 1, 0, 0),
                'in_sample_end': dt.datetime(2018, 1, 4, 0, 0),
                'sample_end': dt.datetime(2018, 1, 5, 0, 0)
            },
            'sample_2': {
                'sample_start': dt.datetime(2018, 1, 2, 0, 0),
                'in_sample_end': dt.datetime(2018, 1, 5, 0, 0),
                'sample_end': dt.datetime(2018, 1, 6, 0, 0)
            },
            'sample_3': {
                'sample_start': dt.datetime(2018, 1, 3, 0, 0),
                'in_sample_end': dt.datetime(2018, 1, 6, 0, 0),
                'sample_end': dt.datetime(2018, 1, 7, 0, 0)
            }
        }
        assert division == division_dict
        return division

if __name__ == "__main__":
    test = TestPypeline()
    division = test.test_divide_dates()
