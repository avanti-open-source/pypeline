import os
import pytest
import pandas as pd

from pypeline import Pypeline
from pypeline.config import TEST_DIR


class TestDataReader:
    """Test case for the class AddTimeSeries
    """
    def test_add_data(self):
        """
        Having a .csv file with many columns, in this example the first
        two columns will be used to build the index of the series and
        other column is chosen to constitute the values of the series.
        """
        # Usage.
        strategy = Pypeline()
        path = os.path.join(TEST_DIR,
                            "test_data",
                            "USA500IDXUSD_H1.csv")
        strategy.data.add_data(source='csv',
                               name='USA500',
                               path=path,
                               use_percentage=True,
                               usecols=[0, 1, 5],
                               parse_dates=[[0, 1]])
                
        # Assertions.
        assert len(strategy.data.data) == 1

        # Usage of variables reader.
        data = strategy['USA500']
        strategy.data.add_data_from_variable(data, name='USA500_2')

        # Assertions.
        assert len(strategy.data.data) == 2
        for name, series in strategy.data.data.items():
            assert type(series.index[0] ==
                        pd._libs.tslibs.timestamps.Timestamp)


        return strategy

if __name__ == "__main__":
    test = TestDataReader()
    test.test_add_data_from_csv()

