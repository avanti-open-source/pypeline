import os
import datetime as dt

from pypeline import Pypeline
from pypeline.operations import Mean, StandardDeviation, LinearPlot
from pypeline.config import TEST_DIR


class TestPlots:
    def test_plot_mean(self):
        # Usage.
        strategy = Pypeline()

        # Adding data.
        path1 = os.path.join(TEST_DIR,
                             "test_data",
                             "EURUSD_H1.csv")

        path2 = os.path.join(TEST_DIR,
                             "test_data",
                             "AUDUSD_H1.csv")

        strategy.data.add_data(source='csv',
                               name='EURUSD_volume',
                               path=path1,
                               use_percentage=False,
                               usecols=[0, 1, 6],
                               parse_dates=[[0, 1]])

        strategy.data.add_data(source='csv',
                               name='AUDUSD_volume',
                               path=path2,
                               use_percentage=False,
                               usecols=[0, 1, 6],
                               parse_dates=[[0, 1]])

        # Calculating Mean and Standard Deviation of the timeseries.
        mean = Mean(result_name='mean')
        std = StandardDeviation(result_name='std')
        strategy.add_step(step_operations=[mean, std],
                          apply_to=['EURUSD_volume'])

        groups = {'EURUSD_volume': 'a', 'negative_EURUSD_volume': 'a', 'AUDUSD_volume': 'c',
                   'mean': 'd', 'negative_mean': 'd'}
        plot = LinearPlot(result_name='Multi Plots',
                          name='double',
                          neg_plot={'mean', 'EURUSD_volume'},
                          subplots=True,
                          constants_name=['mean', 'std'],
                          layout=(2, 2),
                          groups_subplot=groups)
        strategy.add_step(step_operations=[plot],
                          apply_to=['EURUSD_volume', 'AUDUSD_volume',
                                    'mean', 'std'])
        strategy.calculate(start_date=dt.datetime(2015, 1, 1),
                           end_date=dt.datetime(2016, 1, 1))


if __name__ == "__main__":
    test = TestPlots()
    test.test_plot_mean()
