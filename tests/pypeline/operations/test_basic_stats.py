import os
import pytest
import pandas as pd
import datetime as dt

from pypeline import Pypeline
from pypeline.operations import Mean, StandardDeviation
from pypeline.config import TEST_DIR


class TestBasicStats:
    def test_mean(self):
        # Usage.
        strategy = Pypeline()

        # Adding data.
        path = os.path.join(TEST_DIR,
                            "test_data",
                            "USA500IDXUSD_H1.csv")
        strategy.data.add_data(source='csv',
                               name='USA500_volume',
                               path=path,
                               use_percentage=False,
                               usecols=[0, 1, 6],
                               parse_dates=[[0, 1]])

        # Calculating Mean and Standard Deviation of the timeseries.
        mean = Mean()
        std = StandardDeviation(result_name='std')
        strategy.add_step(step_operations=[mean, std],
                          apply_to=['USA500_volume'])

        # Run the strategy.
        strategy.calculate(start_date=dt.datetime(2015, 1, 1),
                           end_date=dt.datetime(2016, 1, 1))

        # Assertions.
        assert int(strategy['mean']) == 3123
        assert int(strategy['std']) == 2900
        

