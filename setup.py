from setuptools import setup, find_packages

setup(
    name='pypeline',
    version='0.1',
    description='General purpose backtester. An empty pipeline to build and test any kind of quantitative strategy',
    url='https://gitlab.com/avanti-sf/public/proyectos-internos/canvas',
    download_url='https://gitlab.com/avanti-sf/public/proyectos-internos/canvas.git',
    author='Avanti Financial Services',
    author_email='pauljherrera@gmail.com',
    license='MIT',
    classifiers=[
        'Development Status :: 4 - Beta',
        'Intended Audience :: Developers',
        'Intended Audience :: Science/Research',
        'Intended Audience :: Financial and Insurance Industry',
        'Natural Language :: English',
        'Programming Language :: Python :: 3',
    ],
    keywords='backtesting quant',
    packages=['pypeline',
              'pypeline.operations',
              'pypeline.strategies',
              'pypeline.strategy_operations',
              'pypeline.utils'],
    install_requires=['pandas', 'matplotlib']
)
